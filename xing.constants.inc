<?php

/**
 * @file
 * Define constants used by the Xing module
 */

// Basic API Settings.
define('XING_API_KEY', variable_get('xing_api_key', NULL));
define('XING_API_SECRET', variable_get('xing_api_secret', NULL));

// User Roles constants.
define('XING_ROLE_ADMIN', 'Xing Admin');
define('XING_ROLE_USER', 'Xing User');


// Access constants.
// TODO: Cleanup and rearange the access rights
define('XING_ACCESS_API', 'access xing api');
define('XING_ACCESS_CONFIGURATION', 'access xing configuration');

define('XING_ACCESS_ADMINISTER_ACCOUNTS', 'Administer xing accounts');
define('XING_ACCESS_CREATE_ACCOUNTS',     'Create xing accounts');
define('XING_ACCESS_DELETE_ACCOUNTS',     'Delete xing accounts');
define('XING_ACCESS_VIEW_ACCOUNTS',       'View xing accounts');
define('XING_ACCESS_UPDATE_ACCOUNTS',     'Update xing accounts');

// TODO: This may be the old rights that must be rearanged
define('XING_ACCESS_VIEW_ALL_ACCOUNTS', 'View all xing accounts');
define('XING_ACCESS_EDIT_ALL_ACCOUNTS', 'Edit all xing accounts');
define('XING_ACCESS_VIEW_OWN_ACCOUNTS', 'View own xing accounts');
define('XING_ACCESS_EDIT_OWN_ACCOUNTS', 'Edit own xing accounts');


// Xing Account Types
define('XING_ACCOUNT_TYPE_REGULAR', 'xing_regular_account');

// Account state constants.
define('XING_ACCOUNT_ENABLED', 1);
define('XING_ACCOUNT_DISABLED', 0);
