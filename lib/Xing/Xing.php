<?php

/**
 * This file defines the 'Xing' class. This class is designed to be a
 * simple, stand-alone implementation of the Xing API functions.
 *
 * This class is based upon the work of fiftyMission Inc. for the Xing API.
 * The original library for Xing can be viewed at http://code.google.com/p/simple-linkedinphp/
 *
 * REQUIREMENTS:
 *
 * 1. You must have cURL installed on the server and available to PHP.
 * 2. You must be running PHP 5+.
 *
 * QUICK START:
 *
 * There are two files needed to enable Xing API functionality from PHP; the
 * stand-alone OAuth library, and this Xing class. The latest version of
 * the stand-alone OAuth library can be found on Google Code:
 *
 * http://code.google.com/p/oauth/
 *
 *
 * RESOURCES:
 *
 * REST API Documentation: http://developer.linkedin.com/rest
 *
 * @version 0.1.0 - March 15, 2012
 * @author Matthias Lersch <matthias.lersch@gmail.com>
 * @copyright Copyright 2012, Matthias Lersch
 * @license http://www.opensource.org/licenses/mit-license.php The MIT License
 */

/**
 * Source: http://code.google.com/p/oauth/
 *
 * Rename and move as needed, changing the require_once() call to the correct
 * name and path.
 */
if (!extension_loaded('oauth')) {
  // the PECL OAuth extension is not present, load our third-party OAuth library
  require_once(drupal_get_path('module', 'xing') . '/lib/OAuth/OAuth.php');
} else {
  // the PECL extension is present, which is not compatible with this library
  throw new XingException('Xing: library not compatible with installed PECL OAuth extension.  Please disable this extension to use the Xing library.');
}


class XingAccount extends Xing {

  protected $aid;

  /**
	 * Create a Xing object, used for OAuth-based authentication and
	 * communication with the Xing API.
	 *
	 * @param arr $callbackUrl  => [OPTIONAL] the callback URL
	 *
	 * @return obj
	 *    A new Xing object.
	 */
	public function __construct($account_id) {
    $this->aid = $account_id;
	}

	/**
   * The class destructor.
   *
   * Explicitly clears Xing object from memory upon destruction.
	 */
  public function __destruct() {
    unset($this);
	}
}


/**
 * 'Xing' class declaration.
 *
 * This class provides generalized Xing oauth functionality.
 *
 * @access public
 * @package classpackage
 */
class Xing {
  // api/oauth settings.
  const _API_OAUTH_REALM             = 'https://api.xing.com';
  const _API_OAUTH_VERSION           = '1.0';

  // The default response format from Xing (psible values are xml and json).
  const _DEFAULT_RESPONSE_FORMAT     = 'json';


  // Helper constants used to standardize Xing <-> API communication.
  const _GET_RESPONSE                = 'Response';
  const _GET_TYPE                    = 'Type';


  // API methods.
  const _METHOD_TOKENS               = 'POST';

  // Network API constants.
  /*
   const _NETWORK_LENGTH              = 1000;
   const _NETWORK_HTML                = '<a>';
	*/


  // Response format type constants.
  const _RESPONSE_JSON               = 'JSON';
  const _RESPONSE_JSONP              = 'JSONP';
  const _RESPONSE_XML                = 'XML';

  // Share API constants.
  /*
   const _SHARE_COMMENT_LENGTH        = 700;
   const _SHARE_CONTENT_TITLE_LENGTH  = 200;
   const _SHARE_CONTENT_DESC_LENGTH   = 400;
   */

  // Xing API end-points
	const _URL_ACCESS                  = 'https://api.xing.com/v1/access_token';
	const _URL_API                     = 'https://api.xing.com';
	const _URL_AUTH                    = 'https://api.xing.com/v1/authorize?oauth_token=';
	const _URL_REQUEST                 = 'https://api.xing.com/v1/request_token';
  // const _URL_REVOKE                 = 'https://api.xing.com/uas/oauth/invalidateToken';


	// Library version.
	const _VERSION                     = '0.1.0';

  // oAuth properties.
  protected $callback;

  public $account_id           = NULL;
  protected $token             = NULL;

  // Application properties.
  protected $application_key,
            $application_secret;

  // the format of the data to return
  protected $response_format         = self::_DEFAULT_RESPONSE_FORMAT;

  // last request fields
  public $last_request_headers,
         $last_request_url;

	/**
	 * Create a Xing object, used for OAuth-based authentication and
	 * communication with the Xing API.
	 *
	 * @param string $account_id  => [OPTIONAL] the account id to use for calls
	 * @param arr $callbackUrl   => [OPTIONAL] the callback URL
	 *
	 * @return obj
	 *    A new Xing object.
	 */
	public function __construct($account_id = NULL, $callbackUrl = NULL) {

    $this->setApplicationKey(variable_get('xing_api_key',    NULL));
	  $this->setApplicationSecret(variable_get('xing_api_secret',    NULL));

	  if (!empty($account_id)) {
	    $this->account_id = $account_id;
	    $this->loadAccount();
	  }


	  if (!empty($callbackUrl)) {
	    $this->setCallbackUrl($callbackUrl);
	  }

	}

	/**
   * The class destructor.
   *
   * Explicitly clears Xing object from memory upon destruction.
	 */
  public function __destruct() {
    unset($this);
	}


	private function loadAccount() {

	  if (module_exists('xing_account')) {
	    $account = xing_account_load_by_xing_id($this->account_id);
      if (empty($account)) {
  	    throw new XingException('Xing->loadAccount(): Not a valid or unknown account ID.');
  	  }

	    $this->setToken(json_decode($account->token, true), false);
	  }else{
	    throw new XingException('Xing->loadAccount(): Module xing_account not enabled.');
	  }
	}

  /**
   * Handle user authorize Xing connection.
   */
  public function authorize($return_page = FALSE) {

    global $base_url;

    if ($return_page) {
      $_SESSION['oauth']['xing']['return_page'] = $return_page;
    }

    $this->setCallbackUrl($this->getCallbackUrl() . '?' . XING::_GET_RESPONSE . '=1');

    // check for response from Xing
    $_GET[XING::_GET_RESPONSE] = (isset($_GET[XING::_GET_RESPONSE])) ? $_GET[XING::_GET_RESPONSE] : '';
    if (!$_GET[XING::_GET_RESPONSE]) {

      // Xing hasn't sent us a response, the user is initiating the connection

      // send a request for a Xing access token
      $response = $this->retrieveTokenRequest();
      if ($response['success'] === TRUE) {
        // store the request token
        $_SESSION['oauth']['xing']['request'] = $response['xing'];

        // redirect the user to the Xing authentication/authorisation
        // page to initiate validation.
        drupal_goto(XING::_URL_AUTH . $response['xing']['oauth_token']);
      } else {
        // bad token request
        $this->watchdog('error', 'Request token retrieval failed', $response);
      }
    } else if (!empty($_GET['oauth_problem']) && $_GET['oauth_problem'] == 'user_refused') {

      $this->watchdog('error', 'User refused authentication', NULL);
      // get return page before unseting the session
      $return_page = $_SESSION['oauth']['xing']['return_page'];

      // unset session, we dont nead it any more
      unset($_SESSION['oauth']);

      // redirect the user back to the demo page
      drupal_goto($return_page);

    } else {
      // Xing has sent a response, user has granted permission,

      // take the temp access token, the user's secret and the verifier to
      // request the user's real secret key.
      $response = $this->retrieveTokenAccess(
                           $_SESSION['oauth']['xing']['request']['oauth_token'],
                           $_SESSION['oauth']['xing']['request']['oauth_token_secret'],
                           $_GET['oauth_verifier']);

      if ($response['success'] === TRUE) {
        // the request went through without an error, gather user's 'access' tokens
        $_SESSION['oauth']['xing']['access'] = $response['xing'];
        // set the user as authorized for future quick reference
        $_SESSION['oauth']['xing']['authorized'] = TRUE;

            // Set account ID if not empty.
      if (!empty($_SESSION['oauth']['xing']['access']['user_id'])) {
        $this->account_id = $_SESSION['oauth']['xing']['access']['user_id'];
      }

        // Save the account.
        if (module_exists('xing_account')) {

          $profile = $this->getProfile();

          krumo($profile);

          $new_xing_account = (object) array(
            'xid'   => $this->account_id,
            'token' => json_encode($this->getToken()),
            'display_name' => $profile->display_name,
            'first_name' => $profile->first_name,
          	'last_name' => $profile->last_name,
            'active_email' => $profile->active_email,
            'photo_data' => $profile->photo_urls,
          	'permalink' => $profile->permalink,
          	'status' => XING_ACCOUNT_ENABLED,
          );
          xing_account_save($new_xing_account);
        }


        // get return page before unseting the session
        $return_page = $_SESSION['oauth']['xing']['return_page'];

        // unset session, we dont nead it any more
        unset($_SESSION['oauth']);

        //drupal_set_message(t('Xing account for %first_name %last_name authorised.',array('%first_name' => $profile['first_name'], '%last_name' => $profile['last_name'])), 'status');
        drupal_set_message(t('Xing account for authorised.'), 'status');

        // redirect the user back to the demo page
        drupal_goto($return_page);
      } else {
        // bad token access
        $this->watchdog('error', 'Access token retrieval failed', $response);
      }
    }
  }


	/**
	 * General profile retrieval function.
	 *
	 * @return arr
	 *   Array containing retrieval success, LinkedIn response.
	 */
	public function getProfile() {

	  // Construct and send the request.
	  $query    = self::_URL_API . '/v1/users/' . $this->account_id . '.' . $this->response_format;
	  $response = $this->fetch('GET', $query);
    $response = $this->checkResponse(array(200, 201), $response);

	  // Check for successful request (a 200 response from LinkedIn server)
	  // per the documentation linked in method comments above.
	  return $response['xing']->users[0];
	}



	/**
	 * Used to check whether a response Xing object has the required http_code or not and
	 * returns an appropriate Xing object.
	 *
	 * @param var $http_code_required
	 * 		The required http response from Xing, passed in either as an integer,
	 * 		or an array of integers representing the expected values.
	 * @param arr $response
	 *    An array containing a Xing response.
	 *
	 * @return boolean
	 * 	  TRUE or FALSE depending on if the passed Xing response matches the expected response.
	 */
	private function checkResponse($http_code_required, $response) {
		// check passed data
    if (is_array($http_code_required)) {
		  array_walk($http_code_required, function($value, $key) {
        if (!is_int($value)) {
    			throw new XingException('Xing->checkResponse(): $http_code_required must be an integer or an array of integer values');
    		}
      });
		} else {
		  if (!is_int($http_code_required)) {
  			throw new XingException('Xing->checkResponse(): $http_code_required must be an integer or an array of integer values');
  		} else {
  		  $http_code_required = array($http_code_required);
  		}
		}
		if (!is_array($response)) {
			throw new XingException('Xing->checkResponse(): $response must be an array');
		}

		// check for a match
		if (in_array($response['info']['http_code'], $http_code_required)) {
		  // response found
		  $response['success'] = TRUE;
		} else {
			// response not found
			$response['success'] = FALSE;
			$response['error']   = 'HTTP response from Xing end-point was not code ' . implode(', ', $http_code_required);
		}

		if (!empty($response['xing'])) {
		  $response['xing'] = json_decode($response['xing']);
		}

		return $response;
	}

	/**
	 * General data send/request method.
	 *
	 * @param str $method
	 *    The data communication method.
	 * @param str $url
	 *    The Xing API endpoint to connect with.
	 * @param str $data
	 *    [OPTIONAL] The data to send to Xing.
	 * @param arr $parameters
	 *    [OPTIONAL] Addition OAuth parameters to send to Xing.
	 *
	 * @return arr
	 *    Array containing:
	 *
	 *           array(
	 *             'info'      =>	Connection information,
	 *             'xing'  => Xing response,
	 *             'oauth'     => The OAuth request string that was sent to Xing
	 *           )
	 */
	protected function fetch($method, $url, $data = NULL, $parameters = array()) {
	  // check for cURL
	  if (!extension_loaded('curl')) {
	    // cURL not present
      throw new XingException('Xing->fetch(): PHP cURL extension does not appear to be loaded/present.');
	  }

    try {
	    // generate OAuth values
	    $oauth_consumer  = new OAuthConsumer($this->getApplicationKey(), $this->getApplicationSecret(), $this->getCallbackUrl());
	    $oauth_token     = $this->getToken();
	    $oauth_token     = (!is_null($oauth_token)) ? new OAuthToken($oauth_token['oauth_token'], $oauth_token['oauth_token_secret']) : NULL;
      $defaults        = array(
        'oauth_version' => self::_API_OAUTH_VERSION
      );
	    $parameters    = array_merge($defaults, $parameters);

	    // generate OAuth request
  		$oauth_req = OAuthRequest::from_consumer_and_token($oauth_consumer, $oauth_token, $method, $url, $parameters);
      $oauth_req->sign_request(new OAuthSignatureMethod_HMAC_SHA1(), $oauth_consumer, $oauth_token);

      // start cURL, checking for a successful initiation
      if (!$handle = curl_init()) {
         // cURL failed to start
        throw new XingException('Xing->fetch(): cURL did not initialize properly.');
      }

      // set cURL options, based on parameters passed
	    curl_setopt($handle, CURLOPT_CUSTOMREQUEST, $method);
      curl_setopt($handle, CURLOPT_RETURNTRANSFER, TRUE);
      curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, FALSE);
      curl_setopt($handle, CURLOPT_URL, $url);
      curl_setopt($handle, CURLOPT_VERBOSE, FALSE);

      // configure the header we are sending to Xing - http://developer.linkedin.com/docs/DOC-1203
      $header = array($oauth_req->to_header(self::_API_OAUTH_REALM));
      if (is_null($data)) {
        // not sending data, identify the content type
        $header[] = 'Content-Type: text/plain; charset=UTF-8';
        switch($this->getResponseFormat()) {
          case self::_RESPONSE_JSON:
            $header[] = 'x-li-format: json';
            break;
          case self::_RESPONSE_JSONP:
            $header[] = 'x-li-format: jsonp';
            break;
        }
      } else {
        $header[] = 'Content-Type: text/xml; charset=UTF-8';
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
      }
      curl_setopt($handle, CURLOPT_HTTPHEADER, $header);

      // set the last url, headers
      $this->last_request_url = $url;
      $this->last_request_headers = $header;

      // gather the response
      $return_data['xing']            = curl_exec($handle);
      $return_data['info']            = curl_getinfo($handle);
      $return_data['curl_error']      = curl_error($handle);
      $return_data['oauth']['header'] = $oauth_req->to_header(self::_API_OAUTH_REALM);
      $return_data['oauth']['string'] = $oauth_req->base_string;

      // check for throttling
      if (self::isThrottled($return_data['xing'])) {
        throw new XingException('Xing->fetch(): throttling limit for this user/application has been reached for Xing resource - ' . $url);
      }

      //TODO - add check for NO response (http_code = 0) from cURL

      // close cURL connection
      curl_close($handle);

      // no exceptions thrown, return the data
      return $return_data;
    } catch(OAuthException $e) {
      // oauth exception raised
      throw new XingException('OAuth exception caught: ' . $e->getMessage());
    }
	}

	/**
	 * Get the application_key property.
	 *
	 * @return str
	 *    The application key.
	 */
	public function getApplicationKey() {
	  return $this->application_key;
	}

	/**
	 * Get the application_secret property.
	 *
	 * @return str
	 *    The application secret.
	 */
	public function getApplicationSecret() {
	  return $this->application_secret;
	}

	/**
	 * Get the callback property.
	 *
	 * @return str
	 *    The callback url.
	 */
	public function getCallbackUrl() {
	  return $this->callback;
	}

  /**
	 * Get the response_format property.
	 *
	 * @return str
	 *    The response format.
	 */
	public function getResponseFormat() {
	  return $this->response_format;
	}

	/**
	 * Get the token_access property.
	 *
	 * @return arr
	 *    The access token.
	 */
	public function getToken() {
	  return $this->token;
	}

	/**
	 * Xing ID validation.
	 *
	 * Checks the passed string $id to see if it has a valid Xing ID format,
	 * which is, as of October 15th, 2010:
	 *
	 *   10 alpha-numeric mixed-case characters, plus underscores and dashes.
	 *
	 * @param str $id
	 *    A possible Xing ID.
	 *
	 * @return bool
	 *    TRUE/FALSE depending on valid ID format determination.
	 */
	public static function isId($id) {
	  // check passed data
    if (!is_string($id)) {
	    // bad data passed
	    throw new XingException('Xing->isId(): bad data passed, $id must be of type string.');
	  }

	  $pattern = '/^[a-z0-9_\-]{10}$/i';
	  if ($match = preg_match($pattern, $id)) {
	    // we have a match
	    $return_data = TRUE;
	  } else {
	    // no match
	    $return_data = FALSE;
	  }
	  return $return_data;
	}

	/**
	 * Throttling check.
	 *
	 * Checks the passed Xing response to see if we have hit a throttling
	 * limit:
	 *
	 * http://developer.linkedin.com/docs/DOC-1112
	 *
	 * @param arr $response
	 *    The Xing response.
	 *
	 * @return bool
	 *    TRUE/FALSE depending on content of response.
	 */
	public static function isThrottled($response) {
	  $return_data = FALSE;


    // check the variable
	  if (!empty($response) && is_string($response)) {
	    // we have an array and have a properly formatted Xing response
      // store the response in a temp variable
      $temp_response = self::xmlToArray($response);
  	  if ($temp_response !== FALSE) {
    	  // check to see if we have an error
    	  if (array_key_exists('error', $temp_response) && ($temp_response['error']['children']['status']['content'] == 403) && preg_match('/throttle/i', $temp_response['error']['children']['message']['content'])) {
    	    // we have an error, it is 403 and we have hit a throttle limit
  	      $return_data = TRUE;
    	  }
  	  }
  	}
  	return $return_data;
	}

	/**
	 * Returns the last request header from the previous call to the
	 * Xing API.
	 *
	 * @returns str
	 *    The header, in string format.
	 */
	public function lastRequestHeader() {
	   return $this->last_request_headers;
	}

	/**
	 * Returns the last request url from the previous call to the
	 * Xing API.
	 *
	 * @returns str
	 *    The url, in string format.
	 */
	public function lastRequestUrl() {
	   return $this->last_request_url;
	}

	/**
	 * Manual API call method, allowing for support for un-implemented API
	 * functionality to be supported.
	 *
	 * @param str $method
	 *    The data communication method.
	 * @param str $url
	 *    The Xing API endpoint to connect with - should NOT include the
	 *    leading https://api.linkedin.com/v1.
	 * @param str $body
	 *    [OPTIONAL] The URL-encoded body data to send to Xing with the request.
	 *
	 * @return arr
	 * 		Array containing retrieval information, Xing response. Note that you
	 * 		must manually check the return code and compare this to the expected
	 * 		API response to determine  if the raw call was successful.
	 */
	public function raw($method, $url, $body = NULL) {
	  if (!is_string($method)) {
	    // bad data passed
		  throw new XingException('Xing->raw(): bad data passed, $method must be of string value.');
	  }
	  if (!is_string($url)) {
	    // bad data passed
		  throw new XingException('Xing->raw(): bad data passed, $url must be of string value.');
	  }
	  if (!is_null($body) && !is_string($url)) {
	    // bad data passed
		  throw new XingException('Xing->raw(): bad data passed, $body must be of string value.');
	  }

    // construct and send the request
	  $query = self::_URL_API . '/v1' . trim($url);
	  return $this->fetch($method, $query, $body);
	}

  /**
	 * Access token retrieval.
	 *
	 * Request the user's access token from the Xing API.
	 *
	 * @param str $token
	 *    The token returned from the user authorization stage.
	 * @param str $secret
	 *    The secret returned from the request token stage.
	 * @param str $verifier
	 *    The verification value from Xing.
	 *
	 * @return arr
	 *    The Xing OAuth/http response, in array format.
	 */
	public function retrieveTokenAccess($token, $secret, $verifier) {
	  // check passed data
    if (!is_string($token) || !is_string($secret) || !is_string($verifier)) {
      // nothing passed, raise an exception
		  throw new XingException('Xing->retrieveTokenAccess(): bad data passed, string type is required for $token, $secret and $verifier.');
    }

    // start retrieval process
	  $this->setToken(array('oauth_token' => $token, 'oauth_token_secret' => $secret));
    $parameters = array(
      'oauth_verifier' => $verifier
    );
    $response = $this->fetch(self::_METHOD_TOKENS, self::_URL_ACCESS, NULL, $parameters);
    parse_str($response['xing'], $response['xing']);

    /**
	   * Check for successful request (a 200 response from Xing server)
	   */
    if ($response['info']['http_code'] == 200 || $response['info']['http_code'] == 201) {
      // tokens retrieved
      $this->setToken($response['xing']);

      // set the response
      $return_data            = $response;
      $return_data['success'] = TRUE;
    } else {
      // error getting the request tokens
       $this->setToken(NULL);

      // set the response
      $return_data            = $response;
      $return_data['error']   = 'HTTP response from Xing end-point was not code 200/201';
      $return_data['success'] = FALSE;
    }
    return $return_data;
	}

	/**
	 * Request token retrieval.
	 *
	 * Get the request token from the Xing API.
	 *
	 * @return arr
	 *    The Xing OAuth/http response, in array format.
	 */
	public function retrieveTokenRequest() {
    $parameters = array(
      'oauth_callback' => $this->getCallbackUrl()
    );
    $response = $this->fetch(self::_METHOD_TOKENS, self::_URL_REQUEST, NULL, $parameters);
    parse_str($response['xing'], $response['xing']);

    /**
	   * Check for successful request (a 200 response from Xing server)
	   * per the documentation linked in method comments above.
	   */
    if (($response['info']['http_code'] == 200 || $response['info']['http_code'] == 201) && (array_key_exists('oauth_callback_confirmed', $response['xing'])) && ($response['xing']['oauth_callback_confirmed'] == 'true')) {
      // tokens retrieved
      $this->setToken($response['xing']);

      // set the response
      $return_data            = $response;
      $return_data['success'] = TRUE;
    } else {
      // error getting the request tokens
      $this->setToken(NULL);

      // set the response
      $return_data = $response;
      if ((array_key_exists('oauth_callback_confirmed', $response['xing'])) && ($response['xing']['oauth_callback_confirmed'] == 'true')) {
        $return_data['error'] = 'HTTP response from Xing end-point was not code 200 or 201';
      } else {
        $return_data['error'] = 'OAuth callback URL was not confirmed by the Xing end-point';
      }
      $return_data['success'] = FALSE;
    }
    return $return_data;
	}

	/**
	 * User authorization revocation.
	 *
	 * Revoke the current user's access token, clear the access token's from
	 * current Xing object. The current documentation for this feature is
	 * found in a blog entry from April 29th, 2010:
	 *
	 *   http://developer.linkedin.com/community/apis/blog/2010/04/29/oauth--now-for-authentication
	 *
	 * @return arr
	 *    Array containing retrieval success, Xing response.
	 */
	public function revoke() {
	  // construct and send the request
	  $response = $this->fetch('GET', self::_URL_REVOKE);

	  /**
	   * Check for successful request (a 200 response from Xing server)
	   * per the documentation linked in method comments above.
	   */
    return $this->checkResponse(200, $response);
	}

	/**
	 * Set the application_key property.
	 *
	 * @param str $key
	 *    The application key.
	 */
	public function setApplicationKey($key) {
	  $this->application_key = $key;
	}

	/**
	 * Set the application_secret property.
	 *
	 * @param str $secret
	 *    The application secret.
	 */
	public function setApplicationSecret($secret) {
	  $this->application_secret = $secret;
	}

	/**
	 * Set the callback property.
	 *
	 * @param str $url
	 *    The callback url.
	 */
	public function setCallbackUrl($url) {
	  $this->callback = $url;
	}

	/**
	 * Set the token property.
	 *
	 * @return arr $token
	 *    The Xing OAuth token.
	 */
	public function setToken($token, $safe_token = TRUE) {
    // check passed data
    if (!is_null($token) && !is_array($token)) {
      // bad data passed
      throw new XingException('Xing->setToken(): bad data passed, $token_access should be in array format.');
    }
    if (!empty($this->account_id) && $safe_token && module_exists('xing_account')) {
      // save token.
      $account = xing_account_load_by_xing_id($this->account_id);

      $account->token = json_encode($token);
      xing_account_save($account);
    }
    // set token
    $this->token = $token;
	}

	/**
	 * Converts passed XML data to an array.
	 *
	 * @param str $xml
	 *    The XML to convert to an array.
	 *
	 * @return arr
	 *    Array containing the XML data.
	 * @return bool
	 *    FALSE if passed data cannot be parsed to an array.
	 */
	public static function xmlToArray($xml) {
	  // check passed data
    if (!is_string($xml)) {
	    // bad data possed
      throw new XingException('Xing->xmlToArray(): bad data passed, $xml must be a non-zero length string.');
	  }

	  $parser = xml_parser_create();
	  xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
    xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
    if (xml_parse_into_struct($parser, $xml, $tags)) {
	    $elements = array();
      $stack    = array();
      foreach ($tags as $tag) {
        $index = count($elements);
        if ($tag['type'] == 'complete' || $tag['type'] == 'open') {
          $elements[$tag['tag']]               = array();
          $elements[$tag['tag']]['attributes'] = (array_key_exists('attributes', $tag)) ? $tag['attributes'] : NULL;
          $elements[$tag['tag']]['content']    = (array_key_exists('value', $tag)) ? $tag['value'] : NULL;
          if ($tag['type'] == 'open') {
            $elements[$tag['tag']]['children'] = array();
            $stack[count($stack)] = &$elements;
            $elements = &$elements[$tag['tag']]['children'];
          }
        }
        if ($tag['type'] == 'close') {
          $elements = &$stack[count($stack) - 1];
          unset($stack[count($stack) - 1]);
        }
      }
      $return_data = $elements;
	  } else {
	    // not valid xml data
	    $return_data = FALSE;
	  }
	  xml_parser_free($parser);
    return $return_data;
  }
  private function watchdog($type, $message, $response = NULL) {

    if (!empty($response)) {
      $message = t('<b>!message</b><br /><br /> RESPONSE:<br /> <pre> !response </pre><br /><br />XING OBJ:<br /><pre> !object </pre>',
      array(
        '!message'  => $message,
      	'!response' => print_r($response, TRUE),
      	'!object'   => print_r($this, TRUE),
      ));
    }else{
      $message = t('<b>!message</b><br /><br /> <pre> !object </pre>',
      array(
        '!message'  => $message,
      	'!response' => print_r($response, TRUE),
      	'!object'   => print_r($this, TRUE),
      ));
    }

    drupal_set_message($message, $type);

    if ($type == 'error') {
      watchdog('Xing CLASS', $message, NULL, WATCHDOG_ERROR);
    }else{
      watchdog('Xing CLASS', $message, NULL, WATCHDOG_WARNING);
    }
  }
}
/**
 * 'XingException' class declaration.
 *
 * This class extends the base 'Exception' class.
 *
 * @access public
 * @package classpackage
 */
class XingException extends Exception {}

