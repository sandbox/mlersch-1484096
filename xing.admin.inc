<?php
/**
 * @file
 * Xing Configuration page callbacks and helper functions
 */


/**
 * Xing Basic Configuration
 */
function xing_configuration_page() {

  $form = drupal_get_form('xing_configuration_form');
  $output  = drupal_render($form);

  return $output;
}
function xing_configuration_form($form, $form_state) {
  $form = array();

  $form['xing_info'] = array(
    '#type' => 'markup',
    '#markup' => 'At the moment, the Xing API is in a "closed beta".<br/> If you are in the "closed beta" group, you get the keys here: <a href="https://dev.xing.com/" target="_blank">https://dev.xing.com/</a>',
  );

  $form['xing_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Xing Consumer key'),
    '#default_value' => XING_API_KEY,
    '#size' => 100,
    '#maxlength' => 500,
    '#required' => TRUE,
    );

  $form['xing_api_secret'] = array(
    '#type' => 'textfield',
    '#title' => t('Xing Consumer secret'),
    '#default_value' => XING_API_SECRET,
    '#size' => 100,
    '#maxlength' => 500,
    '#required' => TRUE,
    );



  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}
function xing_configuration_form_submit($form, &$form_state) {

  system_settings_form_submit($form, $form_state);

}

