<?php

/**
 * @file
 * Theme implementation for non-fields of a xing_account
 *
 * Available variables:
 * - $name: The ID of the field.
 * - $label: The field label.
 * - $value: The value of the field
 */

?>
<div class="field field-name-<?php print $name ?> field-type-text">
  <div class="field-label"><?php print $label ?>:&nbsp;</div>
  <div class="field-item"><?php print $value ?></div>
</div>
