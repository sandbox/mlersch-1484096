<?php

/**
 * @file
 * Theme implementation for profile images of a xing_account
 *
 * Available variables:
 * - $name: The ID of the field.
 * - $label: The field label.
 * - $value: The URLs of the Images.
 * - $size: the Image size to display.
 */

?>
<div class="field field-name-<?php print $name ?>">
  <img src="<?php print $value->{$size} ?>" class="img-size-<?php print $size ?>"/>
</div>
