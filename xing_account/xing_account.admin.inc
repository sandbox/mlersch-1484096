<?php

/**
 * @file
 * Xing Account administrative functions and pages.
 */


/**
 * Menu callback; List all xing_account types available.
 */
function xing_account_overview_types() {
  foreach (xing_account_types() as $type => $info) {
    $type_url_str = str_replace('_', '-', $type);
    $label = t('View @type', array('@type' => $info->name));
    $items[] = l($label, 'admin/structure/xing_accounts/manage/' . $type_url_str);
  }

  return theme('item_list', array('items' => $items));
}

/**
 * Menu callback; xing_account information page.
 *
 * @param object $xing_account_type
 * 	 The Xing account type object @see xing_account_types()
 */
function xing_account_information($xing_account_type) {
  return $xing_account_type->name . ': ' . $xing_account_type->description;
}


/**
 * Menu callback; Xing Account Management
 */
function xing_account_config_page() {

  $form = drupal_get_form('xing_account_authorize_form');
  $output  = drupal_render($form);


  $accounts = db_select('xing_account', 'x')
    ->fields('x', array('aid', 'xid', 'first_name', 'last_name', 'status', 'created'))
    ->execute()
    ->fetchAllAssoc('aid', PDO::FETCH_ASSOC);

  foreach ($accounts as $key => &$account) {
    $account['xid'] = l($account['xid'], 'xing_account/' . $account['aid']);
    $account['status'] = $account['status'] == XING_ACCOUNT_ENABLED ? t('Enabled') : t('Disabled');
    $account['op'] = l($account['aid'], 'xing/account/' . $account['aid']);
    unset($account['token']);
  }
  reset($accounts);

  $headers = array(
    t('Account'),
    t('Xing ID'),
    t('First name'),
    t('Last name'),
    t('Status'),
    t('Created'),
    t('Actions'),
  );

  $output .= theme('table', array(
  	'header' => $headers,
    'rows'   => $accounts,
    'empty'  => t('There are no authorized Xing accounts.'),
  ));

  return $output;

}

/**
 * A simple Xing account authorize form.
 *
 * @param array $form
 * 	 Nested array of form elements that comprise the form.
 *
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 *
 * @return array
 *   Nested array of form elements.
 */
function xing_account_authorize_form($form, $form_state) {

  $form['#action'] = url('xing/api/authorize');

  $form['contact_information'] = array(
    '#markup' => t('You must log off from <b>XING</b> to add a new account,<br/>
                    otherwise the currently logged in user will be added.<br/>
                    Go on, if this is OK for you, but remember: a account<br/>
                    added this way is automatically logged in to <b>XING</b>.<br/>
                    Better you !link now!',
                    array(
                    	'!link' => l(t('log off from <b>Xing</b>'),
                                       'https://www.xing.com/app/user?op=logout',
                                        array(
                                          'html' => TRUE,
                                          'attributes' => array('target' => '_blank'),
                                        )),
                    )));


  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add a new Xing account'),
  );

  return $form;
}
