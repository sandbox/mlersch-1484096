<?php

/**
 * @file
 * The Xing Account entity controler class based on
 * DrupalDefaultEntityController.
 */

class XingAccountController extends DrupalDefaultEntityController {

  /**
   * Enter description here ...
   * @param object $xing_account
   * @return Ambiguous
   */
  public function save($xing_account) {
    $transaction = db_transaction();

    try {

      // Determine if we will be inserting a new xing account.
      if (empty($xing_account->aid) && !empty($xing_account->xid)) {

        // Try to find account by xid and load data.
        $account_search = xing_account_load_by_xing_id($xing_account->xid);
        if (!empty($account_search)) {
          foreach ($xing_account as $key => $val) {
            $account_search->{$key} = $val;
          }
          $xing_account = $account_search;
        }
      }
      $xing_account->is_new = empty($xing_account->aid);

      // Set bundle type if not set.
      if (empty($xing_account->type)) {
        $xing_account->type = XING_ACCOUNT_TYPE_REGULAR;
      }

      // Set the timestamp fields.
      if (empty($xing_account->created)) {
        $xing_account->created = REQUEST_TIME;
      }
      $xing_account->changed = REQUEST_TIME;

      $xing_account->revision_timestamp = REQUEST_TIME;
      $update_xing_account = TRUE;

      // Give modules the opportunity to prepare field data for saving.
      field_attach_presave('xing_account', $xing_account);

      // When saving a new xing account revision, unset any existing
      // $xing_account->vid to ensure a new revision will actually be created
      // and store the old revision ID in a separate property for xing account
      // hook implementations.
      if (!$xing_account->is_new && !empty($xing_account->revision) && $xing_account->vid) {
        $xing_account->old_vid = $xing_account->vid;
        unset($xing_account->vid);
      }

      // If this is a new xing account...
      if ($xing_account->is_new) {

        // Save the new xing account.
        drupal_write_record('xing_account', $xing_account);

        // Save the initial revision.
        $this->saveRevision($xing_account);

        $op = 'insert';
      }
      else {

        // Save the updated xing account.
        drupal_write_record('xing_account', $xing_account, 'aid');

        // If a new xing account revision was requested, save a new record
        // for that; otherwise, update the xing account revision record that
        // matches the value of $xing_account->vid.
        if (!empty($xing_account->revision)) {
          $this->saveRevision($xing_account);
        }
        else {
          $this->saveRevision($xing_account, TRUE);
          $update_xing_account = FALSE;
        }

        $op = 'update';
      }

      // If the revision ID is new or updated, save it to the xing account.
      if ($update_xing_account) {
        db_update('xing_account')
        ->fields(array('vid' => $xing_account->vid))
        ->condition('aid', $xing_account->aid)
        ->execute();
      }

      // Save fields.
      $function = 'field_attach_' . $op;
      $function('xing_account', $xing_account);

      module_invoke_all('entity_' . $op, $xing_account, 'xing_account');

      // Clear internal properties.
      unset($xing_account->is_new);

      // Ignore slave server temporarily to give time for the saved order to be
      // propagated to the slave.
      db_ignore_slave();

      return $xing_account;
    }
    catch (Exception $e) {
      $transaction->rollback();
      watchdog_exception('xing_account', $e, NULL, WATCHDOG_ERROR);
      return FALSE;
    }
  }

  /**
   * Saves an xing account revision with the uid of the current user.
   *
   * @param object $xing_account
   *   The fully loaded xing_account object.
   * @param bool $update
   *   TRUE or FALSE indicating whether or not the existing revision should be
   *     updated instead of a new one created.
   */
  function saveRevision($xing_account, $update = FALSE) {

    // Update the existing revision if specified.
    if ($update) {
      drupal_write_record('xing_account_revision', $xing_account, 'vid');
    }
    else {
      // Otherwise insert a new revision. This will automatically update
      // $xing_account to include the vid.
      drupal_write_record('xing_account_revision', $xing_account);
    }

  }

  /**
   * Deletes multiple xing_accounts by ID.
   *
   * @param array $aids
   *   An array of xing_account IDs to delete.
   *
   * @return bool
   *   TRUE on success, FALSE otherwise.
   */
  public function delete($aids) {
    if (!empty($aids)) {
      $xing_accounts = $this->load($aids, array());

      $transaction = db_transaction();

      try {
        db_delete('xing_account')
        ->condition('aid', $aids, 'IN')
        ->execute();

        db_delete('xing_account_revision')
        ->condition('aid', $aids, 'IN')
        ->execute();

        foreach ($xing_accounts as $xing_account_id => $xing_account) {
          field_attach_delete('xing_account', $xing_account);
        }

        // Ignore slave server temporarily to give time for the
        // saved xing_account to be propagated to the slave.
        db_ignore_slave();
      }
      catch (Exception $e) {
        $transaction->rollback();
        watchdog_exception('xing_account', $e, NULL, WATCHDOG_ERROR);
        return FALSE;
      }

      module_invoke_all('entity_delete', $xing_account, 'xing_account');

      // Clear the page and block and xing_account caches.
      cache_clear_all();
      $this->resetCache();
    }

    return TRUE;
  }

  /**
   * Create a new xing_account object.
   *
   * @param const $type
   *   The entity bundle type
   *
   * @return object
   * 	 A blank new xing_account object.
   */
  public function create($type = XING_ACCOUNT_TYPE_REGULAR) {
    return (object) array(
      'aid' => '',
      'type' => $type,
      'xid' => '',
      'first_name' => '',
      'last_name' => '',
      'token' => '',
      'status' => 1,

    );
  }

}
