<?php

/**
 * @file
 * Default theme implementation to display a xing_account
 *
 * Available variables:
 * - $fields: The Fields of the Xing account, ordered by their weight.
 *
 * Other variables:
 * - $xing_account: The full Xing account object.
 *   Contains data that may not be safe.
 * - $view_mode: View mode, e.g. 'full', 'teaser'...
 */

?>

<p>
<?php
foreach ($fields as $field) {
  echo render($field);
}

?>
</p>
