<?php

/**
 * @file
 * Xing Account functions for pages.
 */

/**
 * Menu page callback
 */
function xing_account_add_page() {
  $item = menu_get_item();
  $links = system_admin_menu_block($item);

  foreach ($links as $link) {
    $items[] = l($link['title'], $link['href'], $item['localized_options']) . ': ' . filter_xss_admin($link['description']);
  }
  return theme('item_list', array('items' => $items));
}

/**
 * Form builder; Displays the xing account add/edit form.
 *
 * @param array $form
 * @param array $form_state
 * @param object $xing_account
 *   The xing account object to edit, which may be brand new.
 */
function xing_account_form($form, &$form_state, $xing_account) {

  // Set the id and identify this as an xing account edit form.
  $form['#id'] = 'xing-account-form';

  // Save the xing account for later, in case we need it.
  $form['#xing_account'] = $xing_account;
  $form_state['xing_account'] = $xing_account;

  // Common fields.
  $form['xid'] = array(
    '#type' => 'textfield',
    '#title' => t('Xing ID'),
    '#default_value' => $xing_account->xid,
    '#weight' => -5,
    '#required' => TRUE,
  );

  $form['revision'] = array(
    '#access' => user_access(XING_ACCESS_ADMINISTER_ACCOUNTS),
    '#type' => 'checkbox',
    '#title' => t('Create new revision'),
    '#default_value' => 0,
  );

  // Add the buttons.
  $form['buttons'] = array();
  $form['buttons']['#weight'] = 100;
  $form['buttons']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 5,
    '#submit' => array('xing_account_form_submit'),
  );
  if (!empty($xing_account->aid)) {
    $form['buttons']['delete'] = array(
      '#access' => user_access(XING_ACCESS_DELETE_ACCOUNTS),
      '#type' => 'submit',
      '#value' => t('Delete'),
      '#weight' => 15,
      '#submit' => array('xing_account_form_delete_submit'),
    );
  }

  $form['#validate'][] = 'xing_account_form_validate';

  field_attach_form('xing_account', $xing_account, $form, $form_state);

  return $form;
}


function xing_account_form_validate($form, &$form_state) {
  $xing_account = $form_state['xing_account'];

  // Field validation.
  field_attach_form_validate('xing_account', $xing_account, $form, $form_state);
}


function xing_account_form_submit($form, &$form_state) {
  global $user;

  $xing_account = &$form_state['xing_account'];

  // Set the xing account's uid if it's being created at this time.
  if (empty($xing_account->uid)) {
    $xing_account->uid = $user->uid;
  }

  $xing_account->xid = $form_state['values']['xid'];
  $xing_account->revision = $form_state['values']['revision'];

  // Notify field widgets.
  field_attach_submit('xing_account', $xing_account, $form, $form_state);

  // Save the xing account.
  xing_account_save($xing_account);

  // Notify the user.
  drupal_set_message(t('Xing account saved.'));

  $form_state['redirect'] = 'xing_account/' . $xing_account->aid;
}


function xing_account_form_delete_submit($form, &$form_state) {
  $destination = array();
  if (isset($_GET['destination'])) {
    $destination = drupal_get_destination();
    unset($_GET['destination']);
  }
  $xing_account = $form['#xing_account'];
  $form_state['redirect'] = array('xing_account/' . $xing_account->aid . '/delete', array('query' => $destination));
}

/**
 * Form bulder; Asks for confirmation of xing_account deletion.
 */
function xing_account_delete_confirm($form, &$form_state, $xing_account) {
  $form['#xing_account'] = $xing_account;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['aid'] = array('#type' => 'value', '#value' => $xing_account->aid);
  return confirm_form($form,
    t('Are you sure you want to delete %title?', array('%title' => $xing_account->xid)),
    'xing_account/' . $xing_account->aid,
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Executes xing_account deletion.
 */
function xing_account_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $xing_account = xing_account_load($form_state['values']['aid']);
    xing_account_delete($form_state['values']['aid']);
    watchdog('xing_account', '@type: deleted %title.', array('@type' => $xing_account->type, '%title' => $xing_account->xid));

    $types = xing_account_types();
    drupal_set_message(t('@type %title has been deleted.', array('@type' => $types[$xing_account->type]->name, '%title' => $xing_account->xid)));
  }

  $form_state['redirect'] = '<front>';
}


/**
 * Displays an xing_account.
 *
 * @param object $xing_account
 *   The xing_account object to display.
 *
 * @param string $view_mode
 *   The view mode we want to display.
 *
 * @return array
 *   An array as expected by drupal_render().
 */
function xing_account_page_view($xing_account, $view_mode = 'full') {
  // Remove previously built content, if exists.
  $xing_account->content = array();

  if ($view_mode == 'teaser') {
    $xing_account->content['title'] = array(
      '#markup' => filter_xss($xing_account->display_name),
      '#weight' => -5,
    );
  }

  // Build fields content.
  $fields = array();
  field_attach_prepare_view('xing_account', array($xing_account->aid => $xing_account), $view_mode);
  entity_prepare_view('xing_account', array($xing_account->aid => $xing_account));
  $fields = field_attach_view('xing_account', $xing_account, $view_mode);

  // Load the extra field infos and current settings
  // (like weight, and display status).
  $extra_fields_info = field_info_extra_fields('xing_account', 'xing_regular_account', 'display');
  $extra_fields_settings = field_extra_fields_get_display('xing_account', 'xing_regular_account', $view_mode);
  // Add data to the extra fields if field is visible.
  // TODO: Check if this is the right way to do this!
  foreach ($extra_fields_info as $field_id => $field_data) {
    if (!empty($extra_fields_settings[$field_id]['visible'])) {
      switch ($field_id) {
        case 'photo_data':
          $fields[$field_id] = array(
            '#name'   => $field_id,
            '#label'  => $field_data['label'],
            '#theme'  => 'xing_account_photo',
          	'#value'  => $xing_account->{$field_id},
            '#weight' => $extra_fields_settings[$field_id]['weight'],
            '#size'		=> 'maxi_thumb',
          );
          break;
        default:
          $fields[$field_id] = array(
            '#name'   => $field_id,
            '#label'  => filter_xss($field_data['label']),
            '#theme'  => 'xing_account_field',
          	'#value'  => filter_xss($xing_account->{$field_id}),
            '#weight' => $extra_fields_settings[$field_id]['weight'],
          );
      }
    }
  }

  // Clean and then sort the fields by weight.
  unset($fields['#entity_type']);
  unset($fields['#bundle']);
  unset($fields['#pre_render']);
  uasort($fields, '_xing_account_sort_weight');

  // Set the theme and data.
  $xing_account->content += array(
    '#theme'        => 'xing_account',
    '#xing_account' => $xing_account,
    '#fields'       => $fields,
    '#view_mode'    => $view_mode,
    '#language'     => NULL,
  );


  return $xing_account->content;
}


/**
 * Menu callback; presents the xing_account editing form, or redirects
 * to delete confirmation.
 *
 * @param object $xing_account
 *   The xing_account object to edit.
 */
function xing_account_page_edit($xing_account) {
  $types = xing_account_types();
  drupal_set_title(t('<em>Edit @type</em> @title', array('@type' => $types[$xing_account->type]->name, '@title' => $xing_account->xid)), PASS_THROUGH);

  return drupal_get_form($xing_account->type . '_xing_account_form', $xing_account);
}

/**
 * Menu page callback
 */
function xing_account_add($type) {
  global $user;

  $types = xing_account_types();
  $type = isset($type) ? str_replace('-', '_', $type) : NULL;
  if (empty($types[$type])) {
    return MENU_NOT_FOUND;
  }

  $xing_account = entity_get_controller('xing_account')->create($type);

  drupal_set_title(t('Create @name', array('@name' => $types[$type]->name)), PASS_THROUGH);
  return drupal_get_form($type . '_xing_account_form', $xing_account);
}


function _xing_account_sort_weight($a, $b) {
  $a_weight = (is_array($a) && isset($a['#weight'])) ? $a['#weight'] : 0;
  $b_weight = (is_array($b) && isset($b['#weight'])) ? $b['#weight'] : 0;
  if ($a_weight == $b_weight) {
    return 0;
  }
  return ($a_weight < $b_weight) ? -1 : 1;
}
